<?php
/**
 * hideUpdateAdmin Plugin for LimeSurvey
 *
 * @author Denis Chenu <http://sondages.pro>
 *
 * @copyright 2015 Denis Chenu <http://sondages.pro>
 * @license AGPL v3 ( GNU AFFERO GENERAL PUBLIC LICENSE )
 * @version 1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with hideUpdateAdmin.  If not, see <http://www.gnu.org/licenses/>.
 */
class hideUpdateAdmin extends PluginBase
{
  protected $storage = 'DbStorage';
  static protected $name = 'hideUpdateAdmin';
  static protected $description = 'Hide unstable udate.';

  public function init()
  {
    $this->subscribe('afterPluginLoad');
  }
  public function afterPluginLoad()
  {
    // Control if we are in an admin page, register everywhere even is not needed
    $oRequest=$this->pluginManager->getAPI()->getRequest();
    $sController=Yii::app()->getUrlManager()->parseUrl($oRequest);
    if(substr($sController, 0, 5)=='admin')
    {
      App()->clientScript->registerCss('hideunstable',"#update-alert.unstable-update{display:none}");
      
    }
  }

  private function getParam($sParam,$default=null)
  {
    $oRequest=$this->pluginManager->getAPI()->getRequest();
    if($oRequest->getParam($sParam))
      return $oRequest->getParam($sParam);
    $sController=Yii::app()->getUrlManager()->parseUrl($oRequest); // This don't set the param according to route always : TODO : fix it (maybe neede $routes ?)
    $aController=explode('/',$sController);
    if($iPosition=array_search($sParam,$aController))
      return isset($aController[$iPosition+1]) ? $aController[$iPosition+1] : $default;
    return $default;
  }
}
